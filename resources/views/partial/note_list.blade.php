<div class="row bg-primary">
    <div class="col-xs-2">Name</div>
    <div class="col-xs-2">Title</div>
    <div class="col-xs-3">Description</div>
    <div class="col-xs-2">Date</div>
    <div class="col-xs-3">{{$action_name}}</div>
</div>
@foreach($notes as $note)
    <div class="row">
        <div class="col-xs-2">{{$note->user->name}}</div>
        <div class="col-xs-2">{{$note->title}}</div>
        <div class="col-xs-3">{{$note->description}}</div>
        <div class="col-xs-2">{{$note->$date}}</div>
        <div class="col-xs-3">
        @if($note->can_be_modified)
            @foreach($actions as $action)
                <a class="btn btn-{{$action['type']}} btn-sm pull-right" href="{{route($action['action'],['id' => $note->id])}}">
                    <i class="fa fa-{{$action['icon']}}" aria-hidden="true"></i>    
                </a>
            @endforeach
        @endif
    </div>
</div>
@endforeach
