@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <a role="button" class="btn btn-primary pull-center" href="{{route('home')}}"> Back to Notes List</a>
        </div>
    @if(count($notes))
        <div class="col-xs-8 col-xs-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                        Deleted Notes
                </div>

                <div class="panel-body">
                     @include('partial.note_list',[
                        'notes'       => $notes,
                        'date'        => 'deleted_at',
                        'action_name' => 'Undelete',
                        'actions' => [[
                            'action' => 'undelete_note',
                            'icon'   => 'plus',
                            'type'   => 'success'
                        ]]   
                    ])
                </div>
            </div>
        </div>
    @endif
    </div>
</div>
@endsection
