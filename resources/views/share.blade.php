@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-8 col-xs-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{$note->title}}
                </div>

                <div class="panel-body">
                    {{$note->description}} 
                </div>
            </div>
        </div>
        <div class="col-xs-8 col-xs-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Share Note 
                </div>

                <div class="panel-body">
                    <form method="POST" action={{route('share_action')}}>
                    <select class="user_select" name="users[]" multiple>
                        @foreach($users as $id => $name)
                            <option value="{{$id}}">{{$name}}</option>
                        @endforeach
                    </select>
                    <input type="hidden" value={{$note->id}} name="note_id"></input> 
                    {{ csrf_field() }}
                    <a role="button" class="btn btn-default" href="{{route('home')}}"> Cancel </a>
                    <input class="btn btn-primary" type="submit" value="Share"></input> 
                    </form> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script src="{{ asset('js/tokenize2.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.user_select').tokenize2();
            $('.user_select').on('tokenize:select', function(container){
                    $(this).tokenize2().trigger('tokenize:search', [$(this).tokenize2().input.val()]);
            });
        });
    </script>
@endsection
@section('css')
    <link href="{{ asset('css/tokenize2.css') }}" rel="stylesheet">
@endsection

