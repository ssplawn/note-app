@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Note
                </div>
                <form action={{route('save',['id'=> $note->id])}}>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input class="form-control" id="title" name="title" value="{{$note->title}}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" 
                                name="description"  rows="4">{{$note->description}}</textarea>
                        </div>
                    </div>
                    <a role="button" class="btn btn-default" href="{{route('home')}}"> Cancel </a>
                    <input class="btn btn-primary" type="submit" value="Save"></input> 
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
