@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add a Note
                </div>
                <form action={{route('save')}}>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input class="form-control" id="title" name="title" placeholder="Title">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" 
                                name="description" placeholder="Description" rows="4"></textarea>
                        </div>
                    </div>
                    <a role="button" class="btn btn-default" href="{{route('home')}}"> Cancel </a>
                    <input class="btn btn-primary" type="submit" value="Save"></input> 
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
