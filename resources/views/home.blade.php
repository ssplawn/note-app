@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <a role="button" class="btn btn-primary pull-center" href="{{route('add')}}"> Add a Note </a>
            <a role="button" class="btn btn-default pull-center" href="{{route('undelete')}}"> Undelete a Note </a>
        </div>
    @if(count(\Auth::user()->notes))
        <div class="col-xs-8 col-xs-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                        Your Notes
                </div>
                <div class="panel-body">
                    @include('partial.note_list',[
                        'notes' => $notes,
                        'date'  => 'updated_at',
                        'action_name' => 'Actions',
                        'actions' => [[
                            'action' => 'delete_note',
                            'icon'   => 'minus',
                            'type'   => 'danger'
                        ],[
                            'action' => 'edit_note',
                            'icon'   => 'edit',
                            'type'   => 'primary'
                        ],[
                            'action' => 'share_note',
                            'icon'   => 'share-alt',
                            'type'   => 'success'
                        ]]   
                        
                    ]) 
                </div>
            </div>
        </div>
    @endif
    </div>
</div>
@endsection
