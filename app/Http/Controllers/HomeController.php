<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = \Auth::user()->notes->merge(\Auth::user()->shared_notes);
        return view('home',['notes' => $notes]);
    }
    
    public function add()
    {
        return view('add');
    }

    public function save(Request $request, $id = null)
    {
        $note_data = $request->only(['title','description']);
        if(is_null($id)){
            $note = new Note($note_data);
            \Auth::user()->notes()->save($note);
        }else{
            $note = Note::find($id);
            $note->fill($note_data)->save();
        }
         return redirect('home');
    }

    public function delete(Request $request, $id)
    {
        
        $note = Note::find($id);
        if(is_object($note) && $note->can_be_modified){
            $note->delete();
        }
        return redirect('home');
    }

    public function undeleteList()
    {
        $notes = Note::onlyTrashed()->where('user_id',\Auth::user()->id)->paginate(5);
        return view('undelete',['notes' => $notes]);
    }
    
    public function undelete(Request $request, $id)
    {

        $note = Note::onlyTrashed()->where('id',$id)->first();
        if(is_object($note) && $note->can_be_modified){
            $note->restore();
        }
        return redirect('undelete');
    }

    public function shareNote(Request $request, $id)
    {
        $note = Note::find($id);
        $users = User::where('id','<>',\Auth::user()->id)->get();
        return view('share',['note' => $note, 'users' => $users->pluck('name','id')]);
    }

    public function shareNoteAction(Request $request)
    {
        $note = Note::find($request->note_id);
        if(is_object($note) && $note->can_be_modified){
            $note->shared_users()->syncWithoutDetaching($request->users);
        }
        return redirect('home');
    }

    public function editNote(Request $request, $id)
    {
        $note = Note::find($id);
        return view('edit',['note' => $note]);
    }
}
