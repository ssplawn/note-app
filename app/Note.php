<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Note extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description',
    ];
    protected $dates = ['deleted_at','created_at','updated_at'];

    protected $casts = [
        'can_be_modified' => 'boolean'
    ];
    

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCanBeModifiedAttribute()
    {
        return \Auth::user()->id === $this->user_id;
    }

    public function shared_users()
    {
        return $this->belongsToMany('App\User');
    }
}
