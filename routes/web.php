<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('register', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/add', 'HomeController@add')->name('add');
Route::get('/save/{id?}', 'HomeController@save')->name('save');
Route::get('/delete/{id}', 'HomeController@delete')->name('delete_note');
Route::get('/undelete/{id}', 'HomeController@undelete')->name('undelete_note');
Route::get('/undelete',  'HomeController@undeleteList')->name('undelete');
Route::get('/share/{id}', 'HomeController@shareNote')->name('share_note');
Route::post('/share_action', 'HomeController@shareNoteAction')->name('share_action');
Route::get('/edit/{id}', 'HomeController@editNote')->name('edit_note');
