# The Note Application #



### What is this repository for? ###

The web application will allow users to create, read, update and delete notes. Notes consist of a “title”,
“description”, “created at” date and if the note has been updated, an “updated at” date. A user might
accidentally delete a note so create the ability to restore a deleted note.
Since some of these notes may be of personal nature, a user will be required to login prior to viewing
any notes. Allow a new user to sign up and create a new account. Notes should be persisted to the
user’s account. Keep in mind that the user will need to access their account from various devices.
A user may also want to share a note with another registered user. Build in the functionality to allow a
user to share a note with another other users and have that note display on their view.included in their
note list.

### How do I get set up? ###

* like a typical LAMP stack laravel app
